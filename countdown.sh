#!/usr/bin/env bash
# [[file:../scripts.org::*countdown.sh][countdown.sh:1]]


### ################## ###
### UNDER CONSTRUCTION ###
### ################## ###


if [[ -z "$1" ]]; then
	read -p "Timer for how many minutes? " -e DURATION
else
	DURATION="$1"
fi

DURATION=$(( $DURATION*60 ))
START=$(date +%s)

while [ -1 ]; do
	clear
	NOW=$(date +%s)
	DIF=$(( $NOW-$START ))
	ELAPSE=$(( $DURATION-$DIF ))
	MINS=$(( $ELAPSE/60 ))
	SECS=$(( $ELAPSE - ($MINS*60) ))

	if [ $MINS == 0 ] && [ $SECS == 0 ]; then
			while :; do
				clear
				echo
				echo
				echo "$(printf "    " )" "$(printf "%02d" $MINS)" : "$(printf "%02d" $SECS)" | figlet
				echo
				echo
				mplayer "/home/icepl/Muzyka/[[tmp]]/system sounds/ring.wav" > /dev/null 2>&1;
				sleep 1;
			done
		break
	else
		echo
		echo
		echo "$(printf "    " )" "$(printf "%02d" $MINS)" : "$(printf "%02d" $SECS)" | figlet
		echo
		echo
		sleep 1
	fi
done
# countdown.sh:1 ends here
