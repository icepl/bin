#!/usr/bin/env bash
# [[file:../scripts.org::*fullscreen-class-toggle.sh][fullscreen-class-toggle.sh:1]]


### ################## ###
### UNDER CONSTRUCTION ###
### ################## ###


CLASS_APPEND='FULLSCREEN'
# echo $CLASS_APPEND

ID=$(echo xargs | xdotool search --pid $(xdotool getwindowpid $(xdotool getactivewindow))|tail -1)
# echo $ID
OLD_CLASS=$(xprop -id $ID WM_CLASS | cut -d' ' -f3-)
# echo $OLD_CLASS
CLASS_LAST_WORD=$(xprop -id $ID WM_CLASS | grep -oE '[^ ]+$')
echo $CLASS_LAST_WORD

separate_variable_to_substrings(){
	IN_OLD_CLASS=$(echo $OLD_CLASS | tr ";" "\n")
	i=0;
	for name in $IN_OLD_CLASS
	do
		if [[ "$name" =~ ,$ ]]
		then
			OLD_NAME[i]=$(echo "$name" | sed "s/\(.*\).\{2\}/\1/" | sed 's/^.\{1\}//')
		else
			OLD_NAME[i]=$(echo "$name" | sed "s/\(.*\).\{1\}/\1/" | sed 's/^.\{1\}//')
		fi
		i=$((i+1))
	done
}

add_variable_to_class(){
	xprop -id $ID \
		-f WM_CLASS 32a \
		-set WM_CLASS \
		'"'${OLD_NAME[0]}'", "'${OLD_NAME[1]}'", "'${OLD_NAME[2]}'", "'${OLD_NAME[3]}'", "'${OLD_NAME[4]}'", "'$CLASS_APPEND'"'
}

remove_variable_from_class(){
	xprop -id $ID \
		-f WM_CLASS 32a \
		-set WM_CLASS \
		'"'${OLD_NAME[0]}'", "'${OLD_NAME[1]}'", "'${OLD_NAME[2]}'", "'${OLD_NAME[3]}'", "'${OLD_NAME[4]}'"'
}

main() {
	separate_variable_to_substrings

	if [[ $CLASS_LAST_WORD == '"FULLSCREEN"' ]]
	then
		remove_variable_from_class
	else
		add_variable_to_class
	fi

}

# gen_list() {
# i=0
# for name in ${OLD_NAME[@]}
# do
# 	echo -n '"'${OLD_NAME[i]}'" '
# 	i=$((i+1))
# done
# }
# list=$(gen_list)

main
# fullscreen-class-toggle.sh:1 ends here
