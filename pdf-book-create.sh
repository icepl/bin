#!/bin/bash


source _library-general.sh
source _library-pdf.sh


# SUMMARY: 		create book from pdf file
## DEPEND: 		create_book(), cut_multiple_pages_from_pdf(), refresh_ps3(), helper(), var_info()
### USAGE: 		pdf-book-create.sh <src-file> OR pdf-book-create.sh --all
#### INFO: 		argument --all automatically create books for all pdf files in current folder
##### !!!:


main () {


	if [[ "$1" == '--all' ]]; then
		for FILE in ./*.pdf; do
			echo 5 | pdf-book-create.sh "${FILE}"
		done
	else
		local SRC_FILE="$1"
		local FIRST_PAGES=( "1" )
		local LAST_PAGES=( "$(count_pages_in_pdf_file $SRC_FILE)" )

		if [[ -z $SRC_FILE ]]; then
			helper main "$0"
			var_info SRC_FILE
			echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
		else
			while true; do
				local SRC_FILE_PREFIX=${SRC_FILE%.*}
				local SRC_FILE_SUFIX=${SRC_FILE##*.}
				LIST=("ADD a new ranges of pages" "SHIFT ranges" "CLEAR all ranges" "CHANGE source file" "RUN")

				clear -x
				helper main "$0"
				var_info SRC_FILE

				echo
				cut_multiple_pages_from_pdf $SRC_FILE no no yes no
				echo $'\n\e[1;91m'"CTRL-C to Quit" $'\e[m'

				refresh_ps3
				echo
				COLUMNS=1

				select ITEM in "${LIST[@]}"; do
					case $ITEM in 
						${LIST[0]})
							cut_multiple_pages_from_pdf $SRC_FILE yes no no no
							break
							;;
						${LIST[1]})
							cut_multiple_pages_from_pdf $SRC_FILE no yes no no
							break
							;;
						${LIST[2]})
							unset FIRST_PAGES
							unset LAST_PAGES
							unset SHIFT
							unset SHIFTED_FIRST_PAGES
							unset SHIFTED_LAST_PAGES
							break
							;;
						${LIST[3]})
							COLUMNS=0
							SRC_FILE=$(chose_file '*.pdf')
							unset FIRST_PAGES
							unset LAST_PAGES
							unset SHIFT
							unset SHIFTED_FIRST_PAGES
							unset SHIFTED_LAST_PAGES
							local FIRST_PAGES=( "1" )
							local LAST_PAGES=( "$(count_pages_in_pdf_file $SRC_FILE)" )
							break
							;;
						${LIST[4]})
							echo -e '\e[1;92mrunning\e[m' 
							if [[ -z $SHIFT ]]; then
								echo -ne '\n' | cut_multiple_pages_from_pdf $SRC_FILE no yes no yes
							else
								cut_multiple_pages_from_pdf $SRC_FILE no no no yes
							fi
							create_book "$SRC_FILE_PREFIX-CUTTED.$SRC_FILE_SUFIX"
							rm "$SRC_FILE_PREFIX-CUTTED.$SRC_FILE_SUFIX"
							break 2
							;;
						*) 
							echo "invalid option --- $REPLY ---"
							;;
					esac
					break
				done
			done
		fi
	fi
}


main "$@"
