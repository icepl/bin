#!/bin/bash


source _library-general.sh
source _library-pdf.sh


# SUMMARY: 		add numbers to pages in pdf file
## DEPEND: 		numbering_pdf_file(), helper(), var_info()
### USAGE: 		pdf-page-numbers.sh <src-file> <side>
#### INFO: 		possible side is:    l=LEFT    c=CENTER    r=RIGHT
##### !!!:


main () {
	local SRC_FILE="$1"
	local SIDE="$2"

	if [[ -z $SRC_FILE || -z $SIDE ]]; then
		helper main "$0"
		var_info SRC_FILE SIDE
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		echo -e '\e[1;92mrunning\e[m' 
		numbering_pdf_file $SRC_FILE $SIDE
	fi
}


main "$@"
