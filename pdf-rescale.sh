#!/bin/bash


source _library-general.sh


# SUMMARY: 		crop margins and rescale to defined size
## DEPEND: 		pdf-crop-margins, pdf-scale, helper(), var_info()
### USAGE: 		pdf-rescale.sh <src-file> <papersize>
#### INFO: 		papersize is based on sizes used in pdf-crop-margins, most common is a4|A4
##### !!!:


main () {
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}
	local PAPERSIZE="$2"

	if [[ -z $SRC_FILE || -z $PAPERSIZE ]]; then
		helper main "$0"
		var_info SRC_FILE PAPERSIZE
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		echo -e '\e[1;92mrunning\e[m' 
		pdf-crop-margins -vsu -gsf $SRC_FILE -o "$SRC_FILE_PREFIX-CROPPED.$SRC_FILE_SUFIX"
		pdf-scale.sh -r $PAPERSIZE "$SRC_FILE_PREFIX-CROPPED.$SRC_FILE_SUFIX" "$SRC_FILE_PREFIX-RESCALED.$SRC_FILE_SUFIX"
		rm "$SRC_FILE_PREFIX-CROPPED.$SRC_FILE_SUFIX"
	fi
}


main "$@"
