#!/bin/bash
# Author: PD
# Date: 2021.12




# SUMMARY: 		ask if directory exist
## DEPEND:
### USAGE: 		ask_if_directory_exist <directory name>
#### INFO: 		return $BASH_CLIPBOARD variable
ask_if_directory_exist() {
	while true; do
		local TMP_DIR="$1"

		case "$( [[ ! -d $TMP_DIR ]] && echo not_exist || echo exist )" in
			exist) 
				return 0
				;;
			not_exist) 
				BASH_CLIPBOARD=$TMP_DIR
				echo $BASH_CLIPBOARD
				return 1
				;;
		esac
	done
}




# SUMMARY: 		ask if file exist
## DEPEND:
### USAGE: 		ask_if_file_exist <file name>
#### INFO: 		return $BASH_CLIPBOARD variable
##### !!!:
ask_if_file_exist() {
	while true; do
		local TMP_FILE="$1"

		case "$( [[ ! -f $TMP_FILE ]] && echo not_exist || echo exist )" in
			exist) 
				return 0
				;;
			not_exist) 
				local BASH_CLIPBOARD=$TMP_FILE
				echo $BASH_CLIPBOARD
				return 1
				;;
		esac
	done
}




# SUMMARY: 		create random directory if not exist (add number at end of passed directory name)
## DEPEND: 		ask_if_directory_exist()
### USAGE: 		create_random_dir_name_if_not_exist <directory name>
create_random_dir_name_if_not_exist() {
	local TMP_DIR="$(echo -n "$1"-; tr -dc 0-9 </dev/urandom | head -c 5 ; echo '')"
	ask_if_directory_exist "$TMP_DIR"

	local RESULT=$(echo $?)
	if [[ $RESULT -eq 0 ]]; then
		create_random_dir_name_if_not_exist "$1"
	fi
}





# SUMMARY: 		create random file name if not exist (add number at end of passed file name)
## DEPEND: 		ask_if_file_exist()
### USAGE: 		create_random_file_name_if_not_exist <file name>
#### INFO:
##### !!!:
create_random_file_name_if_not_exist() {
	local TMP_FILE_PREFIX=${1%.*}
	local TMP_FILE_SUFIX=${1##*.}
	local TMP_FILE="$(echo -n "$TMP_FILE_PREFIX"-; tr -dc 0-9 </dev/urandom | head -c 5 ; echo '').$TMP_FILE_SUFIX"
	ask_if_file_exist "$TMP_FILE"

	local RESULT=$(echo $?)
	if [[ $RESULT -eq 0 ]]; then
		create_random_file_name_if_not_exist "$1"
	fi
}




# SUMMARY: 		insert default ps3 message
## DEPEND:
### USAGE: 		refresh_ps3 [your message]
refresh_ps3 () {
	local MESSAGE="${@:-Please enter your choice}"
	PS3="
:: $MESSAGE: "
}




# SUMMARY: 		chose directory
## DEPEND:
### USAGE: 		TMP_DIR=$(chose_directory)
chose_directory () {
	refresh_ps3 Choose diirectory
	select REPLY in */; do
		break
	done
	echo $REPLY
}




# SUMMARY: 		chose directory
## DEPEND:
### USAGE: 		TMP_FILE=$(chose_file) OR TMP_FILE=$(chose_file '*.pdf')
chose_file () {
	local EXTENSION="${1:-'.* *'}"
	refresh_ps3 Choose file

	for file in $(eval echo "$EXTENSION"); do  
		if [[ -f "$file" ]]; then
			local FILES+=("$file")
		fi
	done

	select REPLY in "${FILES[@]}"; do
		break
	done
	echo $REPLY
}




# SUMMARY: 		info about passed variables
## DEPEND:
### USAGE: 		var_info <var1> <var2> <var3> ...
var_info () {
	echo
	for i in $@; do
		echo current $( [[ $(eval echo \$$(echo $i)) == '' ]] && echo $'\e[1;91m' ||  echo $'\e[1;92m' ) $i = $'\t\e[m'$(eval echo \$$(echo $i))
	done
	# echo $'\n\e[1;91m'"CTRL-C to Quit" $'\e[m'
}




# SUMMARY: 		show help for function
## DEPEND:
### USAGE: 		helper <function name> <script name>
helper () {
	local FUNCTION_NAME="${1:-helper}"
	local FILE="${2:-file}"
	echo $'\e[1;94m'
	awk '/SUMMARY/ {via_line=$0} /'$FUNCTION_NAME'/ {print via_line}' $FILE | uniq
	awk '/DEPEND/ {via_line=$0} /'$FUNCTION_NAME'/ {print via_line}' $FILE | uniq
	echo -n $'\e[1;92m'
	awk '/USAGE/ {via_line=$0} /'$FUNCTION_NAME'/ {print via_line}' $FILE | uniq
	echo -n $'\e[1;94m'
	awk '/INFO/ {via_line=$0} /'$FUNCTION_NAME'/ {print via_line}' $FILE | uniq
	awk '/!!!/ {via_line=$0} /'$FUNCTION_NAME'/ {print via_line}' $FILE | uniq
	echo $'\e[m'
}




# SUMMARY: 		yesno_select
## DEPEND:
### USAGE: 		DEL_TMP_DIR=$(yesno_select)
yesno_select () {
	refresh_ps3 Choose yes or no
	COLUMNS=0
	select REPLY in yes no; do
		case $REPLY in
			"yes")
				echo yes
				;;
			"no")
				echo no
				;;
		esac
		break
	done
}




# SUMMARY: 		synchronize directories
## DEPEND: 		rsync
### USAGE: 		synchronize <type-synchronization=check|backup> <src-path> <dest-path> <file-with-backup-include-list> <file-with-backup-exclude-list>
#### INFO: 		all paths MUST be ABSOLUTE
##### !!!:
synchronize () {
	local TYPE="$1"
	local SRC_PATH="$2"
	local DEST_PATH="$3"
	local BACKUP_INCLUDE_LIST="$4"
	local BACKUP_EXCLUDE_LIST="$5"

	if [[ -z "$BACKUP_INCLUDE_LIST" || -z "$BACKUP_EXCLUDE_LIST" ]]; then
		if [[ $TYPE == 'backup' ]]; then
			rsync -avt --delete --recursive "$SRC_PATH" "$DEST_PATH" 2>/dev/null
		else
			rsync --dry-run -avt --delete --recursive "$SRC_PATH" "$DEST_PATH" 2>/dev/null
		fi
	else
		if [[ $TYPE == 'backup' ]]; then
			rsync -avt --delete --recursive --files-from="$BACKUP_INCLUDE_LIST" --exclude-from="$BACKUP_EXCLUDE_LIST" "$SRC_PATH" "$DEST_PATH"/"${SRC_PATH##*/}" 2>/dev/null
		else
			rsync --dry-run -avt --delete --recursive --files-from="$BACKUP_INCLUDE_LIST" --exclude-from="$BACKUP_EXCLUDE_LIST" "$SRC_PATH" "$DEST_PATH"/"${SRC_PATH##*/}" 2>/dev/null
		fi
	fi
}











