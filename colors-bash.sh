#!/bin/bash


### ################## ###
### UNDER CONSTRUCTION ###
### ################## ###


#   This file echoes a bunch of color codes to the 
#   terminal to demonstrate what's available.  Each 
#   line is the color code of one forground color,
#   out of 17 (default + 16 escapes), followed by a 
#   test use of that color on all nine background 
#   colors (default + 8 escapes).

echo
echo '+-----------+-----------+-----------++-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+'
echo -en "|\e[0m"'\e[0m   bash   ' "\e[0m"
echo -en "|\e[0m"'\e[0m   Hex    ' "\e[0m"
echo -en "|\e[0m"'\e[0m   octal  ' "\e[0m"
echo -en "||\e[0m"'\e[0m  Normal  ' "\e[0m"
echo -en "|\e[1m"'\e[1m   Bold   ' "\e[0m"
echo -en "|\e[2m"'\e[2m   Dim    ' "\e[0m"
echo -en "|\e[3m"'\e[3m  Italic  ' "\e[0m"
echo -en "|\e[4m"'\e[4m Underline' "\e[0m"
echo -en "|\e[5m"'\e[5m Blinking ' "\e[0m"
echo -en "|\e[7m"'\e[7m  Reverse ' "\e[0m"
echo -en "|\e[8m"'\e[0m Invisi\e[8mb\e[0mle' "\e[0m"
echo "|"
echo -n '|    \e     '
echo -n '|   \x1B    '
echo -n '|   \033    '
echo -n '||  \e[0m    '
echo -n '|  \e[1m    '
echo -n '|  \e[2m    '
echo -n '|  \e[3m    '
echo -n '|   \e[4m   '
echo -n '|  \e[5m    '
echo -n '|   \e[7m   '
echo -n '|   \e[8m   '
echo "|"
echo '+-----------+-----------+-----------++-----------+-----------+-----------+-----------+-----------+-----------+-----------+-----------+'


T='gYw'   # The test text

COLORTYPE="____________"
COLORTYPE="\e[7m   Normal   \e[0m"
echo -e "\n$COLORTYPE     40m     41m     42m     43m\
     44m     45m     46m     47m";

for FGs in '    m' '   1m' '  30m' '1;30m' '  31m' '1;31m' '  32m' \
           '1;32m' '  33m' '1;33m' '  34m' '1;34m' '  35m' '1;35m' \
           '  36m' '1;36m' '  37m' '1;37m';
  do FG=${FGs// /}
  echo -en " $FGs \033[$FG  $T  "
  for BG in 40m 41m 42m 43m 44m 45m 46m 47m;
    do echo -en "$EINS \033[$FG\033[$BG  $T  \033[0m";
  done
  echo;
done

COLORTYPE="\e[7mHigh Density\e[0m"
echo -e "\n$COLORTYPE     100m    101m    102m    103m\
    104m    105m    106m    107m";

for FGs in '    m' '   1m' '  90m' '1;90m' '  91m' '1;91m' '  92m' \
           '1;92m' '  93m' '1;93m' '  94m' '1;94m' '  95m' '1;95m' \
           '  96m' '1;96m' '  97m' '1;97m';
  do FG=${FGs// /}
  echo -en " $FGs \033[$FG  $T  "
  for BG in 100m 101m 102m 103m 104m 105m 106m 107m;
    do echo -en "$EINS \033[$FG\033[$BG  $T  \033[0m";
  done
  echo;
done
echo


