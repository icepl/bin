#!/bin/bash


source _library-general.sh
source _library-pdf.sh


# SUMMARY: 		add blank page to file
## DEPEND: 		create_empty_pdf_page(), insert_empty_page_into_pdf_file(), helper(), var_info()
### USAGE: 		pdf-insert-blank-page.sh <src-file> <which-page>
#### INFO:
##### !!!:


main () {
	local SRC_FILE="$1"
	local WHICH_PAGE="$2"
	local BLANK_NAME=$(create_random_file_name_if_not_exist blank.pdf)
	local PDF_WIDTH=$(pdfinfo $SRC_FILE | awk '/Page size/' | awk '{print $3}')
	local PDF_HEIGHT=$(pdfinfo $SRC_FILE | awk '/Page size/' | awk '{print $5}')

	if [[ -z $SRC_FILE || -z $WHICH_PAGE ]]; then
		helper main "$0"
		var_info SRC_FILE WHICH_PAGE
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		echo -e '\e[1;92mrunning\e[m' 
		create_empty_pdf_page $PDF_WIDTH $PDF_HEIGHT $BLANK_NAME
		insert_empty_page_into_pdf_file $WHICH_PAGE $SRC_FILE $BLANK_NAME
		rm $BLANK_NAME
	fi
}


main "$@"
