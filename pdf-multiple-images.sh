#!/usr/bin/env bash
# [[file:../scripts.org::*pdf-multiple-images.sh][pdf-multiple-images.sh:1]]


. _library-general.sh
. _library-pdf.sh


# SUMMARY: 		create pdf file from directory with images
## DEPEND: 		merge_images_into_pdf(), helper(), var_info()
### USAGE: 		pdf-multiple-images.sh <how-many-images-on-page> <src-dir> <dest-file> <yes|no>=delete-temporary-dir?
#### INFO: 		files in tmp directory must look similar to this 	somename-0001.pdf
##### !!!: 		tmp directory will be DELETED if last option is y|yes !!!


main () {
	local HOW_MANY="$1"
	local SRC_DIR="$2"
	local DEST_FILE="$3"
	local DEL_TMP_DIR="$4"

	if [[ -z $HOW_MANY || -z $SRC_DIR || -z $DEST_FILE || -z $DEL_TMP_DIR ]]; then
		helper main "$0"
		var_info HOW_MANY SRC_DIR DEST_FILE DEL_TMP_DIR
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		echo -e '\e[1;92mrunning\e[m' 
		merge_images_into_pdf $HOW_MANY $SRC_DIR $DEST_FILE $DEL_TMP_DIR
	fi
}


main "$@"


# pdf-multiple-images.sh:1 ends here
