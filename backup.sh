#!/bin/bash


source ~/_BACKUP/bin/_library-general.sh


# SUMMARY: 		automatically backup specified directories in lists of files
## DEPEND: 		synchronize()
### USAGE: 		backup.sh
#### INFO: 		at this moment create only local backup
##### !!!:


main () {
	TYPE="${1:-periodically}"
	SYSTEM_NAME="$(lsb_release -i | cut -f 2)"
	MACHINE="$HOSTNAME"
	NOW=$(date +"%Y [%W]")
	# NOW=$(date +"%Y [%W]--%H-%M-%S")
	DEST_DIR="${SYSTEM_NAME} ${MACHINE} ${NOW} $1"
	DEFAULT_BACKUP_LIST_LOCATION="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/'.backup_lists'

	mkdir -p $HOME/_BACKUP/_backup_config/"$DEST_DIR"
	synchronize backup $HOME/.config $HOME/_BACKUP/_backup_config/"$DEST_DIR" $DEFAULT_BACKUP_LIST_LOCATION/'.config -include_list' $DEFAULT_BACKUP_LIST_LOCATION/'.config -exclude_list'
	synchronize backup /etc $HOME/_BACKUP/_backup_config/"$DEST_DIR" $DEFAULT_BACKUP_LIST_LOCATION/'etc -include_list' $DEFAULT_BACKUP_LIST_LOCATION/'etc -exclude_list'
	synchronize backup $HOME $HOME/_BACKUP/_backup_config/"$DEST_DIR" $DEFAULT_BACKUP_LIST_LOCATION/'home -include_list' $DEFAULT_BACKUP_LIST_LOCATION/'home -exclude_list'
}


main "$@"
