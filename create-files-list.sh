#!/bin/bash


# SUMMARY: 		create file list for specific folder
## DEPEND: 		pwd
### USAGE: 		create_files_list [file-name]
#### INFO: 		[file-name] is created in $HOME/bin folder
##### !!!:
create_files_list () {
	local FILE_LIST="${1:-_tmp -include_list}"
	local MIN_DEPTH="${2:-1}"
	local MAX_DEPTH="${3:-2}"
	local DEFAULT_BACKUP_LIST_LOCATION="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"/'.backup_lists'
	local START_DIR=$(echo $(pwd))
	START_DIR=${START_DIR##*/}/

	for file in $(find $(pwd) -mindepth $MIN_DEPTH -maxdepth $MAX_DEPTH |sort -t'/' -k2.2 -k2.1 ); do
		[[ $file == '.' ]] && continue
		[[ $file == '..' ]] && continue
		[[ -h $file ]] && continue
		[[ -d $file ]] && echo "${file##*$START_DIR}/"
		[[ -f $file ]] && echo "${file##*$START_DIR}"
	done > "$DEFAULT_BACKUP_LIST_LOCATION"/"$FILE_LIST"

	echo $FILE_LIST CREATED IN $DEFAULT_BACKUP_LIST_LOCATION
}

create_files_list "$@"
