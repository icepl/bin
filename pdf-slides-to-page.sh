#!/usr/bin/env bash
# [[file:../scripts.org::*pdf-slides-to-page.sh][pdf-slides-to-page.sh:1]]


. _library-general.sh
. _library-pdf.sh


# SUMMARY: 		create pdf file from other pdf file with slides
## DEPEND: 		separate_pdf_file_into_images(), merge_slides_into_pdf(), helper(), var_info()
### USAGE: 		pdf-slides-to-page.sh <src-file> <how-many-images-on-page> <dest-file> <yes|no>=delete-temporary-dir?
#### INFO:
##### !!!:


main () {
	local SRC_FILE="$1"
	local HOW_MANY="$2"
	local DEST_FILE="$3"
	local DEL_TMP_DIR="$4"

	if [[ -z $SRC_FILE || -z $HOW_MANY || -z $DEST_FILE || -z $DEL_TMP_DIR ]]; then
		helper main "$0"
		var_info SRC_FILE HOW_MANY DEST_FILE DEL_TMP_DIR
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		echo -e '\e[1;92mrunning\e[m' 
		separate_pdf_file_into_images "$SRC_FILE"
		# echo $TMP_DIR
		merge_slides_into_pdf $HOW_MANY $TMP_DIR $DEST_FILE $DEL_TMP_DIR
	fi
}


main "$@"









# pdf-slides-to-page.sh:1 ends here
