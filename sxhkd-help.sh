#!/usr/bin/env bash
# [[file:../scripts.org::*sxhkd-help.sh][sxhkd-help.sh:1]]



categories=()
while IFS="" read -r line; do
	if [[ "$line" =~ ^##\  ]]; then # header
		header="$line"
		echo -e "$header"

	elif [[ "$line" =~ ^#[^#] ]]; then # description
		desc="$line"
		echo -e "$desc"

	elif [[ "$line" =~ ^([a-z]|[A-Z]) ]]; then # binding
		binding="$line"
		echo -e "$binding\n"
fi
# done < "$XDG_CONFIG_HOME/sxhkd/sxhkdrc" | less
# done < "$XDG_CONFIG_HOME/sxhkd/sxhkdrc" | less
# done < "~/.config/sxhkd/sxhkdrc" | less
done < "/home/icepl/.config/sxhkd/sxhkdrc" | less
# sxhkd-help.sh:1 ends here
