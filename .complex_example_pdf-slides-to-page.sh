#!/usr/bin/env bash
# [[file:../scripts.org::*pdf-slides-to-page.sh][pdf-slides-to-page.sh:1]]


. _library-general.sh
. _library-pdf.sh


# SUMMARY: 		create pdf file from other pdf file with slides
## DEPEND: 		separate_pdf_file_into_images(), merge_slides_into_pdf(), refresh_ps3(), helper(), var_info()
### USAGE: 		pdf-slides-to-page.sh <src-file> <how-many-images-on-page> <dest-file>
#### INFO:
##### !!!:


run () {
	separate_pdf_file_into_images "$SRC_FILE"
	echo $TMP_DIR
	merge_slides_into_pdf $HOW_MANY $TMP_DIR $DEST_FILE $DEL_TMP_DIR
}


main () {
	local VARLIST=("SRC_FILE" "HOW_MANY" "DEST_FILE" "DEL_TMP_DIR")

	# match arguments
	[[ -f "$1" ]] && local ${VARLIST[0]}="$1" || local ${VARLIST[0]}=''
	[ "$2" -gt 0 ] 2>/dev/null && [ "$2" -eq "$2" ] && local ${VARLIST[1]}="$((10#$2))" || local ${VARLIST[1]}=''
	[[ ! -f "$3" ]] && [[ "${3##*.}" == 'pdf' ]] && local ${VARLIST[2]}="$3" || local ${VARLIST[2]}=''
	local ${VARLIST[3]}='yes'

	# show menu
	local CONDITION="[[ -z $(eval echo \\\$${VARLIST[0]}) || -z $(eval echo \\\$${VARLIST[1]}) || -z $(eval echo \\\$${VARLIST[2]}) ]]"
	if eval "$CONDITION"; then
		refresh_ps3
		while true; do
			LIST=("Choose file" "How many images in one page" "Put destination file name" "Run")
			clear -x
			helper main "$0"
			var_info ${VARLIST[0]} ${VARLIST[1]} ${VARLIST[2]}
			echo
			COLUMNS=1
			select ITEM in "${LIST[@]}"; do
				case $ITEM in 
					${LIST[0]})
						COLUMNS=0
						local ${VARLIST[0]}=$(chose_file)
						refresh_ps3
						;;
					${LIST[1]})
						read -p "        How many images in one page: " ${VARLIST[1]}
						refresh_ps3
						;;
					${LIST[2]})
						read -p "        Put destination file name: " ${VARLIST[2]}
						local TMP_VAR=$(eval echo \$${VARLIST[2]})
						local ${VARLIST[2]}=${TMP_VAR%.*}.pdf
						unset TMP_VAR
						refresh_ps3
						;;
					${LIST[3]})
						eval "$CONDITION" && echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' && sleep 2 && break || echo running && run && break 2
						;;
					*) 
						echo "invalid option --- $REPLY ---"
						;;
				esac
				break
			done
		done
	else
		run
	fi
}

main "$@"









# pdf-slides-to-page.sh:1 ends here
