#!/usr/bin/env bash
# [[file:../scripts.org::*pdf-colors-change.sh][pdf-colors-change.sh:1]]


. _library-general.sh
. _library-pdf.sh


# SUMMARY: 		changes pdf colors palette
## DEPEND: 		pdf_to_gray(), pdf_invert_colors_by_image(), pdf_invert_colors(), helper(), var_info(), refresh_ps3()
### USAGE: 		pdf-colors-change.sh <src-file>
#### INFO:
##### !!!:


main () {
	local SRC_FILE="$1"

	if [[ -z $SRC_FILE ]]; then
		helper main "$0"
		var_info SRC_FILE
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		while true; do
			LIST=("pdf to gray" "pdf invert colors by image" "pdf invert colors")
			# clear -x
			helper main "$0"
			var_info SRC_FILE
			echo $'\n\e[1;91m'"CTRL-C to Quit" $'\e[m'
			echo
			COLUMNS=1
			refresh_ps3
			select ITEM in "${LIST[@]}"; do
				case $ITEM in 
					${LIST[0]})
						echo -e '\e[1;92mrunning\e[m' 
						pdf_to_gray $SRC_FILE
						break 2
						;;
					${LIST[1]})
						echo -e '\e[1;92mrunning\e[m' 
						pdf_invert_colors_by_image $SRC_FILE
						break 2
						;;
					${LIST[2]})
						echo -e '\e[1;92mrunning\e[m' 
						pdf_invert_colors $SRC_FILE
						break 2
						;;
					*) 
						echo "invalid option --- $REPLY ---"
						;;
				esac
				break
			done
		done
	fi
}


main "$@"


# pdf-colors-change.sh:1 ends here
