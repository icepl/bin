#!/bin/bash
# Author: PD
# Date: 2021.11




# SUMMARY: 		count amount of pages in pdf file
## DEPEND:		pdfinfo, awk
### USAGE: 		count_pages_in_pdf_file <pdf_file>
### USAGE: 		PAGES_NR=$(count_pages_in_pdf_file <pdf_file>)
count_pages_in_pdf_file () {
	local FILE_NAME="$1"
	pdfinfo "$FILE_NAME" | grep Pages | awk '{print $2}'
	# )`
}




# # SUMMARY: 		create empty page
# ## DEPEND:		ps2pdf
# ### USAGE: 		crreate_empty_page <page_size=[a4]> <new_empty_page_file_name>
# crreate_empty_pdf_page () {
# 	local SIZE="$1"
# 	local FILE_NAME="$2"
# 	# echo ${FILE_NAME}
# 	echo "" | ps2pdf -sPAPERSIZE="$SIZE" - "$FILE_NAME"
# }




# # SUMMARY: 		create empty page
# ## DEPEND:		ps2pdf
# ### USAGE: 		"USAGE: 	crreate_empty_page <[-p|--page_size [a4|a5|b4|b5]] | [-s|--sizes [width height](in points)]> [-f|--file file_name]"
# crreate_empty_pdf_page() {
# 	local PAGE_SIZE='N'
# 	local SIZES='N'
# 
# 	# match arguments
# 	while [[ $# -gt 0 ]]; do
# 		case $1 in
# 			-p|--page_size)
# 				PAGE_SIZE='Y'
# 				local SIZE="$2"
# 				shift 2 ;;
# 			-s|--sizes)
# 				SIZES='Y'
# 				local WIDTH="$2"
# 				local HEIGHT="$3"
# 				shift 3 ;;
# 			-f|--file)
# 				local DEST_FILE="$2"
# 				shift 2 ;;
# 			-h|--help)
# 				echo "USAGE: 		create_empty_page <[-p|--page_size [a4|a5|b4|b5]] | [-s|--sizes [width height](in points)]> [-f|--file file_name]" ;;
# 			*)
# 				echo "Unknown option $1" $'\n' "USAGE: 		create_empty_page <[-p|--page_size [a4|a5|b4|b5]] | [-s|--sizes [width height](in points)]> [-f|--file file_name]"
# 				return 1 ;;	
# 		esac
# 	done
# 
# 	# check parameters
# 	[[ -z $DEST_FILE ]] && local DEST_FILE='blank.pdf'
# 	WIDTH=$(echo $WIDTH | sed 's/[_A-Za-z\-]*//g')
# 	HEIGHT=$(echo $HEIGHT | sed 's/[_A-Za-z\-]*//g')
# 	
# 	if [[ $PAGE_SIZE == 'Y' && $SIZES == 'Y' ]]; then
# 		echo "Choose one type of sizes"
# 		return 1
# 	elif [[ $SIZES == 'Y' ]]; then
# 		if [[ $WIDTH == '' || $HEIGHT == '' ]]; then
# 			echo Width and Height must be specified
# 			return 1
# 		fi
# 	fi
# 
# 	# execute
# 	if [[ $PAGE_SIZE == 'Y' ]]; then
# 		echo "" | ps2pdf -sPAPERSIZE="$SIZE" - "$DEST_FILE"
# 	else
# 		echo "" | ps2pdf -dFIXEDMEDIA -dDEVICEWIDTHPOINTS=$WIDTH -dDEVICEHEIGHTPOINTS=$HEIGHT - "$DEST_FILE"
# 	fi
# }





# SUMMARY: 		create empty page
## DEPEND:		ps2pdf
### USAGE: 		create_empty_page <papersize | [width height]>
#### INFO: 		papersize=a4|a5|b4|b5     [width height](in points)
##### !!!:
create_empty_pdf_page() {
	if [[ $# -eq 2 ]]; then
		local PAPERSIZE="$1"
		local DEST_FILE="$2"
		echo "" | ps2pdf -sPAPERSIZE="$PAPERSIZE" - "$DEST_FILE"
	else
		local WIDTH="$1"
		local HEIGHT="$2"
		local DEST_FILE="$3"
		WIDTH=$(echo $WIDTH | sed 's/[_A-Za-z\-]*//g')
		HEIGHT=$(echo $HEIGHT | sed 's/[_A-Za-z\-]*//g')
		echo "" | ps2pdf -dFIXEDMEDIA -dDEVICEWIDTHPOINTS=$WIDTH -dDEVICEHEIGHTPOINTS=$HEIGHT - "$DEST_FILE"
	fi
}



# SUMMARY: 		insert page into pdf file in chosen position
## DEPEND: 		count_pages_in_pdf_file, pdftk
### USAGE: 		insert_page_into_file <on_which_page> <source_file> <blank_name>
insert_empty_page_into_pdf_file () {
	local WHICH_PAGE="$1"
	local SRC_FILE="$2"
	local BLANK_NAME="$3"

	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}
	local DEST_FILE="${SRC_FILE_PREFIX}-EMPTY.$SRC_FILE_SUFIX"

	local WHAT="A=$SRC_FILE B=$BLANK_NAME"
	local PAGES_NR=$(count_pages_in_pdf_file "$SRC_FILE")
	
	if [[ $WHICH_PAGE -eq 1 ]]; then
		WHERE="B A$WHICH_PAGE-end"
	elif [[ $WHICH_PAGE -gt $PAGES_NR ]]; then
		WHERE="A1-end B"
	else
		WHERE="A1-$(($WHICH_PAGE-1)) B A$WHICH_PAGE-end"
	fi

	pdftk $WHAT cat ${WHERE} output $DEST_FILE
}




# SUMMARY: 		remove page from pdf file
## DEPEND: 		count_pages_in_pdf_file, pdftk
### USAGE: 		remove_page_from_pdf_file <page_nr> <source_file>
remove_page_from_pdf_file () {
	local WHICH_PAGE="$1"
	local SRC_FILE="$2"

	SRC_FILE_PREFIX=${SRC_FILE%.*}
	SRC_FILE_SUFIX=${SRC_FILE##*.}

	local SRC_FILE_TMP=${SRC_FILE_PREFIX}-in.$SRC_FILE_SUFIX
	mv $SRC_FILE $SRC_FILE_TMP

	local PAGES_NR=$(count_pages_in_pdf_file "$SRC_FILE_TMP")
	local DEST_FILE=${SRC_FILE_PREFIX}.$SRC_FILE_SUFIX

	if [[ $WHICH_PAGE -eq 1 ]]; then
		WHERE="$(($WHICH_PAGE+1))-end"
	elif [[ $WHICH_PAGE -eq $PAGES_NR ]]; then
		WHERE="1-$(($WHICH_PAGE-1))"
	elif [[ $WHICH_PAGE -gt $PAGES_NR ]]; then
		echo OUT OF RANGE
		echo DOCUMENT HAVE $PAGES_NR 'page(s)'
		mv $SRC_FILE_TMP $SRC_FILE
		return 1
	else
		WHERE="1-$(($WHICH_PAGE-1)) $(($WHICH_PAGE+1))-end"
	fi

	pdftk $SRC_FILE_TMP cat $WHERE output $DEST_FILE
	rm $SRC_FILE_TMP
}




# SUMMARY: 		convert text file to pdf
## DEPEND: 		enscript, ps2pdf
### USAGE: 		convert_txt_to_pdf <text_file>
convert_txt_to_pdf () {
	local FILE_NAME="$1"
	[[ $FILE_NAME == *"."* ]] && local FILE_NAME_WO_EXT=${FILE_NAME%.*} || local FILE_NAME_WO_EXT=${FILE_NAME}

	enscript -B $FILE_NAME -o - | ps2pdf - $FILE_NAME_WO_EXT.pdf
}




# SUMMARY: 		change pallete in pdf to gray
## DEPEND: 		gs
### USAGE: 		pdf_to_grey <$SRC_FILE>
pdf_to_gray(){
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}

	gs \
	 -sOutputFile="${SRC_FILE_PREFIX}-GRAY.$SRC_FILE_SUFIX" \
	 -sDEVICE=pdfwrite \
	 -sColorConversionStrategy=Gray \
	 -dProcessColorModel=/DeviceGray \
	 -dCompatibilityLevel=1.4 \
	 -dNOPAUSE \
	 -dBATCH \
	 "$SRC_FILE"
}




# SUMMARY: 		invert colors in pdf
## DEPEND: 		gs
### USAGE: 		pdf_invert_colors <$SRC_FILE>
pdf_invert_colors(){
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}

	gs -q -sDEVICE=pdfwrite -o "${SRC_FILE_PREFIX}-INVERTED.$SRC_FILE_SUFIX" -c '{1 sub neg} settransfer' -f "$SRC_FILE"
}




# SUMMARY: 		invert colors in pdf (but losing pdf structure)
## DEPEND: 		create_random_dir_name_if_not_exist(), pdftoppm, convert
### USAGE: 		pdf_invert_colors_by_image <$SRC_FILE>
pdf_invert_colors_by_image(){
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}
	local TMP_DIR=$(create_random_dir_name_if_not_exist tmp-directory)
	mkdir $TMP_DIR

	pdftoppm "$SRC_FILE" $TMP_DIR/page -png > /dev/null 2>&1

	for file in $TMP_DIR/*.png ; do
		convert -negate $file ${file%.*}-INVERTED.png ;
		rm $file
	done

	convert $TMP_DIR/*-INVERTED.png "${SRC_FILE_PREFIX}-INVERTED.$SRC_FILE_SUFIX"
	rm -rf $TMP_DIR
}




# SUMMARY: 		resize pdf file
## DEPEND: 		gs
### USAGE: 		resize_pdf_file <SRC_FILE> <QUALITY [screen|ebook|prepress|printer|default]>
reduce_size_pdf_file() {
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}
	local QUALITY="$2"

	rm -rf "${SRC_FILE_PREFIX}-RESIZED.$SRC_FILE_SUFIX"

	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/$QUALITY -dNOPAUSE -dQUIET -dBATCH -sOutputFile="${SRC_FILE_PREFIX}-RESIZED.$SRC_FILE_SUFIX" "$SRC_FILE"
}




# SUMMARY: 		numbering pdf file
## DEPEND: 		enscript
### USAGE: 		numbering_pdf_file <SRC_FILE> <SIDE>
#### INFO: 		possible side is:    l=LEFT    c=CENTER    r=RIGHT
numbering_pdf_file() {
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}

	local PAGES_NR=$(count_pages_in_pdf_file $SRC_FILE)
	[[ "$2" == l ]] && local SIDE=$(echo '')
	[[ "$2" == c ]] && local SIDE=$(echo '|')
	[[ "$2" == r ]] && local SIDE=$(echo '||')

	enscript -L1 --header="$SIDE"'Page $% of $=' --output - < <(for i in $(seq "$PAGES_NR"); do echo; done) | ps2pdf - | pdftk "$SRC_FILE" multistamp - output "${SRC_FILE_PREFIX}-NUMBERED.$SRC_FILE_SUFIX"
}




# SUMMARY: 		separare pdf file into images in temporary directory
## DEPEND: 		pdfseparate, create_random_dir_name_if_not_exist()
### USAGE: 		separare_pdf_file_into_image <SRC_FILE>
#### INFO: 		return $TMP_DIR as global variable
separate_pdf_file_into_images() {
	TMP_DIR=$(create_random_dir_name_if_not_exist tmp-directory)
	mkdir $TMP_DIR
	local PAGES_PREFIX='page_'
	local SRC_FILE="$1"

	pdfseparate "$SRC_FILE" $TMP_DIR/$PAGES_PREFIX%04d.pdf
}




# SUMMARY: 		merge multiple slides into one pdf
## DEPEND: 		awk, pdfinfo, create_empty_pdf_page(), pdfjam, pdfunite
### USAGE: 		merge_slides_into_pdf <HOW_MANY_SLIDES_IN_ONE_PAGE> <TMP_DIR> <DEST_FILE> <DEL_TMP_DIR=[yes|y|no|n]>
#### INFO: 		files in tmp directory must look similar to this 	somename-0001.pdf
##### !!!: 		tmp directory will be DELETED if last option is y|yes !!!
merge_slides_into_pdf() {
	local HOW_MANY="$1"
	local TMP_DIR="$2"
	local DEST_FILE="$3"
	local DEL_TMP_DIR="${4:-no}"

	local EXAMPLE_FILE=$(ls -l $TMP_DIR | head -n 2 | tail -n 1 | awk '{print $NF}')
	local PAGES_PREFIX=${EXAMPLE_FILE%%0*}
	local PDF_WIDTH=$(pdfinfo $TMP_DIR/$EXAMPLE_FILE | awk '/Page size/' | awk '{print $3}')
	local PDF_HEIGHT=$(pdfinfo $TMP_DIR/$EXAMPLE_FILE | awk '/Page size/' | awk '{print $5}')
	local FILES_LIST="$(ls -l $TMP_DIR | tail -n +2 | awk '{print $NF}' )"
	local FILES_NR=$(ls "$TMP_DIR" | wc -l)
	local BLANK_NAME=$(create_random_file_name_if_not_exist $TMP_DIR/blank.pdf)

	for (( i=1; i<=$FILES_NR; i=i+$HOW_MANY )); do
		FILES=''
		for (( j=$i; j<$HOW_MANY+$i; j=j+1 )); do
			if [[ $j -gt $FILES_NR ]]; then
				create_empty_pdf_page $PDF_WIDTH $PDF_HEIGHT $BLANK_NAME
				FILE="$BLANK_NAME "
			else
				FILE="$TMP_DIR/${PAGES_PREFIX}$(printf "%04d\n" $j).pdf "
			fi
			FILES+=$FILE
		done
		pdfjam $FILES --nup 1x$HOW_MANY --no-landscape --paper a4paper --quiet --outfile $TMP_DIR/concated_$(printf "%04d" $i).pdf
	done

	pdfunite $TMP_DIR/concated_*.pdf $DEST_FILE
	[[ ${DEL_TMP_DIR,,} == 'yes' || ${DEL_TMP_DIR,,} == 'y' ]] && rm -Rf $TMP_DIR || rm -Rf $TMP_DIR/concated_*.pdf && rm -Rf $BLANK_NAME
}




# SUMMARY: 		merge images into pdf
## DEPEND: 		create_random_dir_name_if_not_exist(), merge_slides_into_pdf()
### USAGE: 		merge_images_into_pdf <HOW_MANY_SLIDES_IN_ONE_PAGE> <TMP_DIR> <DEST_FILE> <DEL_TMP_DIR=[yes|y|no|n]>
merge_images_into_pdf () {
	local HOW_MANY="$1"
	local SRC_DIR="$2"

	local DEST_FILE="$3"
	local DEL_TMP_DIR="${4:-no}"
	local FORMATS="*.jpeg *.jpg *.png"

	TMP_DIR=$(cd "$SRC_DIR"; create_random_dir_name_if_not_exist tmp-directory)
	mkdir $SRC_DIR/$TMP_DIR

	cd $SRC_DIR
	for image in $FORMATS; do
		echo $image
		convert $image $TMP_DIR/${image%.*}.pdf 2> /dev/null
	done
	merge_slides_into_pdf $HOW_MANY $TMP_DIR $DEST_FILE $DEL_TMP_DIR
	mv $DEST_FILE ../$DEST_FILE
	cd ..
}




# SUMMARY: 		cut pages from pdf file
## DEPEND: 		pdftk
### USAGE: 		cut_pages_from_pdf <first-page> <last-page> <src-file> <dest-file>
#### INFO:
##### !!!:
cut_pages_from_pdf () {
	local FIRST_PAGE="$1"
	local LAST_PAGE="$2"
	local SRC_FILE="$3"
	local DEST_FILE="$4"

	pdftk $SRC_FILE cat $FIRST_PAGE-$LAST_PAGE output $DEST_FILE
}




# SUMMARY: 		create book from pdf file
## DEPEND: 		pdf-crop-margins, pdfbook2, pdftk
### USAGE: 		create_book <src-file>
#### INFO:
##### !!!:
create_book () {
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	SRC_FILE_PREFIX=${SRC_FILE_PREFIX%%-CUTTED*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}

	pdf-crop-margins -s -u -gsf -a4 -10 0 -10 0 "$SRC_FILE" -o "$SRC_FILE_PREFIX-CROPPED.$SRC_FILE_SUFIX"
	pdfbook2 -n "$SRC_FILE_PREFIX-CROPPED.$SRC_FILE_SUFIX"
	pdftk "$SRC_FILE_PREFIX-CROPPED-book.$SRC_FILE_SUFIX" rotate 1-end oddsouth output "$SRC_FILE_PREFIX-BOOK.$SRC_FILE_SUFIX"
	rm "$SRC_FILE_PREFIX-CROPPED.$SRC_FILE_SUFIX"
	rm "$SRC_FILE_PREFIX-CROPPED-book.$SRC_FILE_SUFIX"
}




# SUMMARY: 		cut multiple pages from pdf file
## DEPEND: 		create_random_dir_name_if_not_exist(), cut_pages_from_pdf(), pdfunite
### USAGE: 		cut_multiple_pages_from_pdf <src-file> [add-ranges=yes|no] [shift-ranges=yes|no] [show-info=yes|no] [execute=yes|no]
#### INFO:
##### !!!:
cut_multiple_pages_from_pdf () {
	local SRC_FILE="$1"
	local SRC_FILE_PREFIX=${SRC_FILE%.*}
	local SRC_FILE_SUFIX=${SRC_FILE##*.}
	local TMP_DIR=$(create_random_dir_name_if_not_exist tmp-directory)
	local ADD_RANGES="${2:-yes}"
	local SHIFT_RANGES="${3:-yes}"
	local SHOW_INFO="${4:-yes}"
	local EXECUTE="${5:-yes}"

	local ERROR_PREV='echo -e "\n\n\t\e[1;91m"Current value must be greaater then previous"\n\e[m"'
	local ERROR_ZERO='echo -e "\n\n\t\e[1;91m"Current value must be greaater then zero"\n\e[m"'
	local ERROR_LAST='echo -e "\n\n\t\e[1;91m"Last page must be greaater then first page"\n\e[m"'
	local PAK="read -n 1 -r -s -p $'\tPress any key to continue...\n'"

	if [[ $ADD_RANGES == 'yes' ]]; then
		clear -x
		echo -e '\n'"        First Page and Last Page of Range"'\n'
		i=${#FIRST_PAGES[@]}
		while true; do
			ERROR=0
			tput sc
			read -p "        Range $((${i}+1)) FIRST: " FIRST_PAGE
			[[ $ERROR -eq 0 ]] && [[ -z $FIRST_PAGE ]] && break
			[[ $ERROR -eq 0 ]] && [[ $FIRST_PAGE -lt 1 ]] && eval $ERROR_ZERO && ERROR=1
			[[ $ERROR -eq 0 ]] && [[ $FIRST_PAGE -le ${FIRST_PAGES[i-1]} ]] 2>/dev/null && eval $ERROR_PREV && eval $PAK && ERROR=1
			[[ $ERROR -eq 0 ]] && [[ $FIRST_PAGE -le ${LAST_PAGES[i-1]} ]] 2>/dev/null && eval $ERROR_PREV && eval $PAK && ERROR=1
			tput rc
			tput ed
			[[ $ERROR -eq 0 ]] && read -p "        Range $((${i}+1)) FIRST: $FIRST_PAGE      Range $((${i}+1)) LAST: " LAST_PAGE
			[[ $ERROR -eq 0 ]] && [[ -z $LAST_PAGE ]] && break
			[[ $ERROR -eq 0 ]] && [[ $LAST_PAGE -lt 1 ]] && eval $ERROR_ZERO && eval $PAK && ERROR=1
			[[ $ERROR -eq 0 ]] && [[ $LAST_PAGE -le ${LAST_PAGES[i-1]} ]] 2>/dev/null && eval $ERROR_PREV && eval $PAK && ERROR=1
			[[ $ERROR -eq 0 ]] && [[ $FIRST_PAGE -gt $LAST_PAGE ]] 2>/dev/null && eval $ERROR_PREV && eval $PAK && ERROR=1
			[[ $ERROR -eq 1 ]] && tput rc && tput ed

			[[ $ERROR -eq 0 ]] && FIRST_PAGES+=($FIRST_PAGE) && LAST_PAGES+=($LAST_PAGE) && ((i+=1))
		done
	fi

	if [[ $SHIFT_RANGES == 'yes' ]]; then
		echo -e "\e[0;33m"
		read -p "        PAGE in document: " DOC_PAGE
		[[ -z $DOC_PAGE ]] && DOC_PAGE=0 && FILE_PAGE=0 || while true; do read -p "        PAGE in file: " FILE_PAGE; [[ -n $FILE_PAGE ]] && break; done
		echo -e "\e[m"

		SHIFT=$(($DOC_PAGE-$FILE_PAGE))
		for i in ${FIRST_PAGES[@]}; do
			SHIFTED_FIRST_PAGES+=($(($i-$SHIFT)))
		done
		for i in ${LAST_PAGES[@]}; do
			SHIFTED_LAST_PAGES+=($(($i-$SHIFT)))
		done
	fi

	if [[ $SHOW_INFO == 'yes' ]]; then
		echo -e "\n"    '        ---------------------------      '
		printf '%s\t'   '          FIRST PAGES OF RANGE :=        '"  ${FIRST_PAGES[@]}"
		printf '\n'
		printf '%s\t'   '          LAST PAGES OF RANGE  :=        '"  ${LAST_PAGES[@]}"
		echo -e "\n"    '        ---------------------------      '"\e[0;33m"
		printf '%s\t'   '          SHIFT                :=        '"  $SHIFT"
		echo -e "\n\e[m"'        ---------------------------      '"\e[0;36m"
		printf '%s\t'   '          FIRST PAGES OF RANGE :=        '"  ${SHIFTED_FIRST_PAGES[@]}"
		printf '\n'
		printf '%s\t'   '          LAST PAGES OF RANGE  :=        '"  ${SHIFTED_LAST_PAGES[@]}"
		echo -e "\n\e[m"'        ---------------------------      '"\n"
	fi

	if [[ $EXECUTE == 'yes' && ${#SHIFTED_FIRST_PAGES[@]} -ne 0 ]]; then
		mkdir $TMP_DIR
		for ((i=0; i<=((${#SHIFTED_FIRST_PAGES[@]}-1)); i=i+1)); do
			local DEST_FILE="$TMP_DIR/${SRC_FILE_PREFIX}-$(printf "%04d\n" $i).$SRC_FILE_SUFIX"
			cut_pages_from_pdf ${SHIFTED_FIRST_PAGES[i]} ${SHIFTED_LAST_PAGES[i]} $SRC_FILE $DEST_FILE
		done

		for i in "$TMP_DIR/${SRC_FILE_PREFIX}-*.$SRC_FILE_SUFIX"; do
			pdfunite $i "$TMP_DIR/${SRC_FILE_PREFIX}-CUTTED.$SRC_FILE_SUFIX"
		done
		mv "$TMP_DIR/${SRC_FILE_PREFIX}-CUTTED.$SRC_FILE_SUFIX" "${SRC_FILE_PREFIX}-CUTTED.$SRC_FILE_SUFIX"
		rm -rf $TMP_DIR
	fi
}










