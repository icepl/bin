#!/usr/bin/env bash


source _library-general.sh
source _library-pdf.sh


# SUMMARY: 		change quality of pdf file
## DEPEND: 		reduce_size_pdf_file(), helper(), var_info()
### USAGE: 		pdf-reduce_size.sh <src-file> <quality>
#### INFO: 		posible qualities is:    screen (72 dpi)    ebook (150 dpi)    prepress (300 dpi)    printer(300 dpi)     default
##### !!!:


main () {
	local SRC_FILE="$1"
	local QUALITY="$2"

	if [[ -z $SRC_FILE || -z $QUALITY ]]; then
		helper main "$0"
		var_info SRC_FILE QUALITY
		echo -e '\n\n\e[1;91msomething not exist\n\n\e[m' 
	else
		echo -e '\e[1;92mrunning\e[m' 
		reduce_size_pdf_file $SRC_FILE $QUALITY
	fi
}


main "$@"
